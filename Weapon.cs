using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System;
using UnityEngine.InputSystem;
using StarterAssets;
using Random = UnityEngine.Random;


public class Weapon : MonoBehaviour
{
    public Action<int, int> OnAmmoChaged;

    [SerializeField] private Transform center;
    private float nextTimeToFire = 0.3f;
    private bool isNextFireAvailable = true;
    private int baseDamage = 15;

    private int ammo;
    [SerializeField] private int magazine;

    private AudioSource audioSource;
    [SerializeField] private AudioClip fireSfx;
    [SerializeField] private AudioClip emptyMagazineSfx;
    public float timeToReload = 1.7f;
    [SerializeField] private AudioClip reloadSfx;
    private bool isReloading = false;

    private Animator animator;
    private InputAction mousePosition;


    private Creature owner;

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
        ammo = magazine;
        OnAmmoChaged?.Invoke(ammo, magazine);

        animator = GetComponent<Animator>();
    }

    public void Initialize(Creature owner, InputAction mousePosition)
    {
        this.owner = owner;
        this.mousePosition = mousePosition;
    }

    public IEnumerator Fire()
    {
        if (isNextFireAvailable)
        {
            isNextFireAvailable = false;
            if (ammo >= 1)
            {
                ammo--;
                audioSource.clip = fireSfx;
                audioSource.Play();

                animator.SetTrigger("Shoot");

                RaycastHit hit;
                // Does the ray intersect any objects excluding the player layer
                Vector3 from = center.position;
                Vector3 to = transform.TransformDirection(Vector3.forward);
                var ray = Camera.main.ScreenPointToRay(mousePosition.ReadValue<Vector2>());

                if (Physics.Raycast(ray, out hit, Mathf.Infinity))
                {
                    Debug.Log(hit.transform.name);
                    if (hit.transform.TryGetComponent<ITakeDamage>(out ITakeDamage takeDamage))
                    {
                        takeDamage.TakeDamage(GetDamage());
                    }
                    Debug.DrawRay(from, to * hit.distance, Color.red, 3f);
                }

                yield return new WaitForEndOfFrame();
                OnAmmoChaged?.Invoke(ammo, magazine);
            }
            else
            {
                audioSource.clip = emptyMagazineSfx;
                audioSource.Play();
                yield return null;
            }
            yield return new WaitForSeconds(nextTimeToFire);
            isNextFireAvailable = true;
        }
        else
        {
            yield return null;
        }
    }

    public int GetDamage()
    {
        int result = (int)(baseDamage * Random.Range(0.7f, 1.2f));
        return result;
    }


    public void Reload()
    {
        if (isReloading == false)
            StartCoroutine(ReloadCoroutine());
    }

    private IEnumerator ReloadCoroutine()
    {
        isReloading = true;
        audioSource.clip = reloadSfx;
        audioSource.Play();
        animator.SetTrigger("Reload");

        yield return new WaitForSeconds(timeToReload);
        ammo = magazine;
        OnAmmoChaged?.Invoke(ammo, magazine);

        isReloading = false;
    }

    private void FixedUpdate()
    {
        Debug.DrawRay(center.position, center.transform.TransformDirection(Vector3.forward) * 1000, Color.white, 0.1f);

    }
}
