﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterPhysics : MonoBehaviour
{
    public void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.isTrigger == true)
        {
            return;
        }

        if (collider.TryGetComponent<Pirate>(out Pirate pirate))
        {
            pirate.State = PirateMovementState.swim;
        }

        Instantiate(ResourceHolder.WaterSplashParticles, collider.transform.position, Quaternion.identity, null);

        if (collider.TryGetComponent<UnderwaterBehavior>(out UnderwaterBehavior waterPhysics) == true)
        {
            waterPhysics.enabled = true;
        }
        else
        {
            collider.gameObject.AddComponent(typeof(UnderwaterBehavior));
        }
    }

    public void OnTriggerExit2D(Collider2D collider)
    {
        if (collider.isTrigger == true)
        {
            return;
        }

        if (collider.TryGetComponent<Pirate>(out Pirate pirate))
        {
            pirate.ComeUpToTheSurface();
        }

    }
}
