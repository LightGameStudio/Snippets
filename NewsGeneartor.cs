﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewsGeneartor : MonoBehaviour
{
    private const int freeVersionArticlesCount = 24;
    private const int paidVersionArticlesCount = 27;

    private List<Article> articles;
    private List<Article> doubleArticles;

    private int articlesCount;
    private const int doublearticlesCount = 1;

    private IEnumerator Start()
    {
        if (UnlockStatus.gameUnlocked == GameUnlocked.Full)
        {
            articlesCount = paidVersionArticlesCount;
        }
        else
        {
            articlesCount = freeVersionArticlesCount;

        }
        yield return new WaitForSecondsRealtime(10f);
        //ReadArticlesFromFile();
        yield return new WaitForSecondsRealtime(1f);
    }

    public void ReadArticlesFromFile()
    {
        articles = new List<Article>();
        doubleArticles = new List<Article>();

        for (int i = 0; i <= articlesCount; i++)
        {
            articles.Add(GetArticleFromFile(i));
        }

        for (int i = 0; i <= doublearticlesCount; i++)
        {
            doubleArticles.Add(NewsManager.Instance.GetDoubleArticle(i, (i + 1) * Random.Range(3, 6 + i)));
        }
        Localization.ClearArticles();
        StartCoroutine(GenerateRandomNew(GetTimeToAddNewspaper()));
    }

    private Article GetArticleFromFile(int number)
    {
        return new Article(Localization.GetArticle("article_caption_" + number.ToString()),
                            Localization.GetArticle("article_text_" + number.ToString()));
    }

    private float GetTimeToAddNewspaper()
    {
        return 23f + Player.Instance.averageAnswerTime * 1.5f + Random.Range(0, 26f);
    }

    private IEnumerator GenerateRandomNew(float deltaTime)
    {
        yield return new WaitForSeconds(deltaTime);

        if (articles.Count > 0)
        {
            int index = Random.Range(0, articles.Count);
            NewsManager.Instance.CreateNewspaper(articles[index]);
            articles.RemoveAt(index);
        }
        StartCoroutine(GenerateRandomNew(GetTimeToAddNewspaper()));
    }

    public void GenerateMomentumNewspaper()
    {
        int articleToShow = Random.Range(0, articles.Count);
        NewsManager.Instance.CreateNewspaper(articles[articleToShow]);
        articles.RemoveAt(articleToShow);
    }
}