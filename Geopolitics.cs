using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum RelationshipLevel
{
    Unforgivable,
    Afraid,
    Enemy,
    Competitor,
    Neutral,
    Favorable,
    Friend,
    Ally
}

public class Geopolitics : AutoCleanupSingleton<Geopolitics>
{
    public bool isSabotageAvailable = false;
    private Dictionary<Tuple<AState, AState>, int> relations;
    private List<AState> states = new List<AState>();

    private int defaultRelationsScore = 1;
    private WarController warController;

    private const int minRelationshipScore = -10;
    private const int maxRelationshipScore = 20;

    public RelationshipLevel GetRelationshipLevel(AState a, AState b)
    {
        int score = GetRelationsScore(a, b);
        if (score >= 17)
        {
            return RelationshipLevel.Ally;
        }
        if (score >= 12)
        {
            return RelationshipLevel.Friend;
        }
        if (score <= -2)
        {
            return RelationshipLevel.Enemy;
        }
        if (score <= -7)
        {
            return RelationshipLevel.Unforgivable;
        }
        else
        {
            return RelationshipLevel.Neutral;
        }
    }


    private void Start()
    {
        TupleComparer<AState, AState> comparer = new TupleComparer<AState, AState>();
        relations = new Dictionary<Tuple<AState, AState>, int>(comparer);
    }


    public void Register(AState newState)
    {
        if (states.Contains(newState))
        {
            return;
        }

        states.Add(newState);

        if (states.Count > 0)
        {
            foreach (AState state in states)
            {
                if (!relations.ContainsKey(new Tuple<AState, AState>(state, newState)) && state != newState)
                {
                    relations.Add(new Tuple<AState, AState>(state, newState), defaultRelationsScore);
                }
            }
        }
    }

    public int GetRelationsScore<TS1, TS2>() where TS1 : AState where TS2 : AState
    {
        int value = 0;
        TS1 a = GetState<TS1>();
        TS2 b = GetState<TS2>();

        relations.TryGetValue(new Tuple<AState, AState>(a, b), out value);
        return value;
    }

    public int GetRelationsScore(AState a, AState b)
    {
        int value = 0;
        relations.TryGetValue(new Tuple<AState, AState>(a, b), out value);
        return value;
    }


    public void ModifyRelations<TS1, TS2>(int value) where TS1 : AState where TS2 : AState
    {
        value = Mathf.Clamp(value, minRelationshipScore, maxRelationshipScore);
        relations[GetTuple<TS1, TS2>()] += value;
    }

    public void ModifyRelations(AState a, AState b, int value)
    {
        value = Mathf.Clamp(value, minRelationshipScore, maxRelationshipScore);
        relations[GetTuple(a, b)] += value;
    }

    public Tuple<AState, AState> GetTuple<TS1, TS2>() where TS1 : AState where TS2 : AState
    {
        var requestedType1 = typeof(TS1);
        var requestedType2 = typeof(TS2);

        Tuple<AState, AState> result = new Tuple<AState, AState>();

        //int value = 0;
        //if (relations.TryGetValue(new Tuple<AState, AState>(), out value))
        //{
        //    return relations[];
        //};

        foreach (KeyValuePair<Tuple<AState, AState>, int> pair in relations)
        {
            if ((pair.Key.First.GetType() == requestedType1 && pair.Key.Second.GetType() == requestedType2) ||
            (pair.Key.First.GetType() == requestedType2 && pair.Key.Second.GetType() == requestedType1))
            {
                return pair.Key;
            }
        }

        return result;
    }

    public Tuple<AState, AState> GetTuple(AState a, AState b)
    {
        Tuple<AState, AState> result = new Tuple<AState, AState>();

        foreach (KeyValuePair<Tuple<AState, AState>, int> pair in relations)
        {
            if ((pair.Key.First == a && pair.Key.Second == b) ||
            (pair.Key.First == b && pair.Key.Second == a))
            {
                return pair.Key;
            }
        }

        return result;
    }

    public TState GetState<TState>() where TState : class
    {
        var requestedType = typeof(TState);

        foreach (var state in states)
        {
            if (!requestedType.IsInstanceOfType(state)) continue;
            var result = state as TState;
            return result;
        }
        return null;
    }

    public bool TryGetState<TState>(out TState result) where TState : AState
    {
        result = GetState<TState>();
        return result != null;
    }

    public bool IsThereState<TState>() where TState : AState
    {
        var result = GetState<TState>();
        return result != null;
    }
}