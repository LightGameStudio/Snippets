using UnityEngine;

public enum BuisnessArea { Food, Culture, Escort, Medicine, Production }

public class Buisness
{
    public string selfName;
    public string nameKey;
    private int maxDailyRevenue;
    public int capital = 1;
    public BuisnessArea area;
    private float risk; // 0.01 < risk < 0.9

    public IJuridicalPerson owner;

    private System.Func<bool> CanEarnMoney;
    private System.Func<int> CalculateGrossRevenue;

    public Buisness(string nameKey, int maxDailyRevenue, float risk01, Character _owner, BuisnessArea buisnessArea,
     System.Func<bool> CheckConditionFunction, System.Func<int> CalculateRevenueFunction = null)
    {
        this.nameKey = nameKey;
        this.maxDailyRevenue = maxDailyRevenue;
        risk = risk01;
        CanEarnMoney = CheckConditionFunction;
        owner = _owner;
        selfName = Localization.GetString("buisness_" + this.nameKey);

        if (CalculateRevenueFunction != null)
        {
            CalculateGrossRevenue = CalculateRevenueFunction;
        }
        else
        {
            CalculateGrossRevenue = CalculateRevenueDefault;
        }
    }

    public Buisness() { }

    public int TickAndGetTax(float tax)
    {
        if (CanEarnMoney() == false)
        {
            return 0;
        }

        int grossRevenue = CalculateGrossRevenue();
        int taxDeduction = (int)(grossRevenue * tax);
        int netRevenue = grossRevenue - taxDeduction;
        capital += netRevenue;

        return taxDeduction;
    }

    public void CalculateRevenueGross() { }

    private int CalculateRevenueDefault()
    {
        float currentRisk = (1.0f - risk) * Random.Range(0.65f, 1.2f);
        currentRisk = Mathf.Clamp(currentRisk, 0.00f, 1.00f);
        int revenue = Random.Range((int)(maxDailyRevenue * currentRisk), maxDailyRevenue);

        return revenue;
    }

    public void Bankruptcy()
    {

    }
}