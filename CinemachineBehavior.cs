﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class CinemachineBehavior : MonoBehaviour
{
    private CinemachineVirtualCamera cinemachine;
    private CinemachineFramingTransposer cinemachineFramingTransposer;


    public float viewSize;
    private float lookaheadTime;
    private readonly float viewSizeZoomed = 1.45f;
    private Transform cameraStartPosition;
    private float deadZoneWidth;
    private float deadZoneHeight;

    [SerializeField] private Collider2D boundary;
    private Vector3 boundaryData;

    private Coroutine zoomOut;

    private bool doZoom = false;

    private float duration;
    private float newSize;
    private float elapsed = 0.0f;

    private float zoomInLimit = 0.8f;
    private float zoomOutLimit = 3.5f;

    private bool doZoomOut = true;

    private void Awake()
    {
        cinemachine = GetComponent<CinemachineVirtualCamera>();
        cinemachineFramingTransposer = cinemachine.GetCinemachineComponent<CinemachineFramingTransposer>();
    }

    private void Start()
    {
        if (QualitySettings.vSyncCount == 0)
        {
            Application.targetFrameRate = 60;
        }

#if UNITY_EDITOR
        QualitySettings.vSyncCount = 0;
        Application.targetFrameRate = 60;
#endif

        viewSize = cinemachine.m_Lens.OrthographicSize;
        lookaheadTime = cinemachineFramingTransposer.m_LookaheadTime;

        deadZoneWidth = cinemachineFramingTransposer.m_DeadZoneWidth;
        deadZoneHeight = cinemachineFramingTransposer.m_DeadZoneHeight;

        SetBoundarySize();

        zoomOutLimit = Mathf.Sqrt(Map.Instance.numColumns * Map.Instance.numRows) / 3.7f;
        zoomInLimit = Mathf.Sqrt(Map.Instance.numColumns * Map.Instance.numRows) / 13.5f;


        cinemachine.m_Lens.OrthographicSize = zoomOutLimit * 0.8f;
        cinemachine.m_Follow = GetCentralHex().transform;
        //cameraStartPosition.position = FindObjectOfType<Camera>().ScreenToWorldPoint(new Vector3(Screen.width / 2, Screen.height / 2, FindObjectOfType<Camera>().nearClipPlane));
    }

    private Hex GetCentralHex()
    {
        return Map.Instance.GetMapCenterHex();
    }

    private void Update()
    {
        if (doZoom)
        {
            ZoomSmooth();
        }
    }

    private void SetBoundarySize()
    {
        float scalePerHexX = 0.48f;
        float scalePerHexY = 0.43f;
        boundary.GetComponent<BoxCollider2D>().size = new Vector2(Map.Instance.numColumns * scalePerHexX, Map.Instance.numRows * scalePerHexY);
    }

    private void ZoomSmooth()
    {
        if (elapsed < duration)
        {
            elapsed += Time.deltaTime;
            float t = Mathf.Clamp01(elapsed / duration);

            cinemachine.m_Lens.OrthographicSize = Mathf.Lerp(cinemachine.m_Lens.OrthographicSize, newSize, t);
        }
    }

    public void Move(Vector3 direction)
    {
        StopZoomOut();
        Vector3 targetPosition = transform.position + direction;
        targetPosition.x = Mathf.Clamp(targetPosition.x, boundary.bounds.min.x, boundary.bounds.max.x);
        targetPosition.y = Mathf.Clamp(targetPosition.y, boundary.bounds.min.y, boundary.bounds.max.y);

        transform.position = Vector3.Lerp(transform.position, targetPosition, Time.deltaTime * 4.5f * cinemachine.m_Lens.OrthographicSize);
    }

    public void SetZoom(float delta)
    {
        if (delta != 0.00f && doZoomOut == true)
        {
            StopZoomOut();
        }
        cinemachine.m_Lens.OrthographicSize =
        Mathf.Clamp(cinemachine.m_Lens.OrthographicSize - delta * 0.7f, zoomInLimit, zoomOutLimit);
    }

    public void ZoomIn(Transform target)
    {
        cinemachine.m_Follow = target;

        cinemachineFramingTransposer.m_DeadZoneHeight = deadZoneHeight;
        cinemachineFramingTransposer.m_DeadZoneWidth = deadZoneWidth;

        ResizeView(viewSizeZoomed, 0.8f * (cinemachine.m_Lens.OrthographicSize / viewSizeZoomed));
        doZoomOut = true;
    }

    public void ZoomOut()
    {
        if (doZoomOut)
        {
            cinemachine.m_Follow = null;

            cinemachineFramingTransposer.m_DeadZoneHeight = 0.0f;
            cinemachineFramingTransposer.m_DeadZoneWidth = 0.0f;

            ResizeView(viewSize, 0.8f * (viewSizeZoomed / cinemachine.m_Lens.OrthographicSize));
        }
    }

    public void StopZoomOut()
    {
        doZoomOut = false;
        cinemachineFramingTransposer.m_DeadZoneHeight = 0.0f;
        cinemachineFramingTransposer.m_DeadZoneWidth = 0.0f;
        cinemachine.m_Follow = null;
    }

    private void ResizeView(float newSize, float duration)
    {
        doZoom = true;
        this.newSize = newSize;
        this.duration = duration;
        elapsed = 0.0f;
    }
}
