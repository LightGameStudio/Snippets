using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using Input = InputWrapper.Input;
#endif

public class Plane : MonoBehaviour
{
    [SerializeField] [Range(550, 1500)] private float forwardSpeed;
    private float forwardSpeedOnRotation;

    [SerializeField] [Range(100, 600)] private float angularSpeed;
    [SerializeField] [Range(50, 300)] private float rotationSharpness;


    [SerializeField] private GameObject waterParticlesPrefab;
    [SerializeField] private ParticleSystem waterParticles;

    [SerializeField] private Transform center;

    private Rigidbody rigidbody;

    public bool doMoveRight;
    public bool doMoveLeft;
    private bool doRotate = false;

    private Vector3 target;
    private Quaternion targetRotation;
    private float currentSpeed;


    private void Awake()
    {
        rigidbody = GetComponent<Rigidbody>();
    }

    private void Start()
    {
        forwardSpeedOnRotation = forwardSpeed * 0.7f;
        currentSpeed = forwardSpeed;
        StartDropWater();
    }

    private void Update()
    {
        HandleMovement();
        ReadInput();
        if (doRotate)
        {
            HandleRotation();
        }

    }

    private void FixedUpdate()
    {
        AFireable fireable = TryToGetRaycastFirable();
        if (fireable != null)
        {
            fireable.StopFire();
        }
    }

    private void HandleRotation()
    {
        transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRotation, Time.deltaTime * rotationSharpness);
        if (targetRotation == transform.rotation)
        {
            doRotate = false;
            currentSpeed = forwardSpeed;
        }
    }

    private void ReadInput()
    {
        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);

            if (touch.phase == TouchPhase.Ended)
            {
                Vector3 targetPosition = new Vector3(touch.position.x, touch.position.y, (Camera.main.farClipPlane + Camera.main.nearClipPlane) * 0.5f);
                targetPosition = Camera.main.ScreenToWorldPoint(targetPosition);
                TurnTo(targetPosition);
            }
        }
    }

    private void HandleMovement()
    {
        transform.Translate(Vector3.forward * currentSpeed * Time.deltaTime);
        // Vector3 targetPosition = transform.position + Vector3.forward * forwardSpeed;
        // transform.position = Vector3.Lerp(transform.position, targetPosition, Time.deltaTime);


        if (doMoveLeft)
        {
            transform.Rotate(Vector3.down * angularSpeed * Time.deltaTime, Space.World);
        }

        if (doMoveRight)
        {
            transform.Rotate(Vector3.up * angularSpeed * Time.deltaTime, Space.World);
        }
    }

    public void TurnBack()
    {
        TurnTo(center.position);
    }


    public void TurnTo(Vector3 newTarget)
    {
        target = newTarget;
        newTarget = new Vector3(newTarget.x, transform.position.y, newTarget.z);

        Quaternion lookRotation = Quaternion.LookRotation(newTarget - transform.position);
        targetRotation = lookRotation;

        doRotate = true;
        currentSpeed = forwardSpeedOnRotation;
    }

    private AFireable TryToGetRaycastFirable()
    {
        AFireable fireable = null;
        RaycastHit[] hits = Physics.RaycastAll(transform.position - transform.forward * 20f, Vector3.down, 3000);

        if (hits.Length != 0)
        {
            foreach (RaycastHit hit in hits)
            {
                if (hit.transform.GetComponent<AFireable>())
                {
                    fireable = hit.transform.GetComponent<AFireable>();
                    break;
                }
            }
        }
        return fireable;
    }

    public void StartDropWater()
    {
        if (waterParticles == null)
        {
            Vector3 waterPosition = new Vector3(transform.position.x, transform.position.y - 50f, transform.position.z - 250f);
            waterParticles = Instantiate(waterParticlesPrefab, waterPosition, Quaternion.identity, transform).GetComponent<ParticleSystem>();
        }
        else
        {
            waterParticles.Play();
        }

        AFireable fireable = TryToGetRaycastFirable();
        if (fireable != null)
        {
            fireable.StopFire();
        }
    }

    public void StopDropWater()
    {
        waterParticles.Stop();
    }
}
