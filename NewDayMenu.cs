using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;
using TMPro;

public class NewDayMenu : MonoBehaviour
{
    [SerializeField] private GameObject newDayMenu;
    [SerializeField] private TextMeshProUGUI title;
    [SerializeField] private TextMeshProUGUI baseText;

    [SerializeField] private GameObject additionalInfoMenu;
    [SerializeField] private TextMeshProUGUI additionalTitle;
    [SerializeField] private TextMeshProUGUI additionalBaseText;

    private BudgetRevenue budgetRevenueToday;
    private PopulationGrowth populationGrowthToday;


    public void ShowNewDayNotification(BudgetRevenue budgetRevenue, PopulationGrowth populationGrowth)
    {
        if (Date.Instance.autoDayTick == true)
        {
            return;
        }

        additionalInfoMenu.SetActive(false);

        budgetRevenueToday = budgetRevenue;
        populationGrowthToday = populationGrowth;

        newDayMenu.SetActive(true);
        baseText.text = Localization.GetString("newDay_newCivils") + ": " + populationGrowth.GetNew().ToString();
        baseText.text += "\n" + Localization.GetString("newDay_peopleDied") + ": " + populationGrowth.GetDied().ToString();
        baseText.text += "\n" + Localization.GetString("newDay_revenue") + ": " + budgetRevenue.GetCommonRevenue().ToString();
        baseText.text += "\n" + Localization.GetString("newDay_budgetSpending") + ": " + budgetRevenue.GetCommonSpending().ToString();
    }

    public void ShowAditionalBudgetInfo()
    {
        LevelAudio.instance.Play(SoundType.Click);
        additionalInfoMenu.SetActive(true);

        additionalTitle.text = Localization.GetString("newDay_additional_budget_title");
        additionalBaseText.text = Localization.GetString("newDay_additional_budget_revenue") + ": " + budgetRevenueToday.individualTaxRevenue.ToString();

        if (budgetRevenueToday.buisnessTaxRevenue != 0)
        {
            additionalBaseText.text += "\n" + Localization.GetString("newDay_additional_budget_buisnessTaxRevenue") + ": " + budgetRevenueToday.buisnessTaxRevenue.ToString();
        }
        if (budgetRevenueToday.prostitutionRevenue != 0)
        {
            additionalBaseText.text += "\n" + Localization.GetString("newDay_additional_budget_prostitutionRevenue") + ": " + budgetRevenueToday.prostitutionRevenue.ToString();
        }


        additionalBaseText.text += "\n" + Localization.GetString("newDay_additional_budget_serviceSpending") + ": " + budgetRevenueToday.serviceSpeding.ToString();
        if (budgetRevenueToday.socialSpeding != 0)
        {
            additionalBaseText.text += "\n" + Localization.GetString("newDay_additional_budget_socialSpending") + ": " + budgetRevenueToday.socialSpeding.ToString();
        }
    }

    public void ShowAditionalPopulationInfo()
    {
        LevelAudio.instance.Play(SoundType.Click);
        additionalInfoMenu.SetActive(true);

        additionalTitle.text = Localization.GetString("newDay_additional_population_title");
        additionalBaseText.text = Localization.GetString("newDay_additional_population_born") + ": " + populationGrowthToday.born.ToString();

        AddDiedString(ref additionalBaseText, "migrants", populationGrowthToday.migrants);
        AddDiedString(ref additionalBaseText, "died", populationGrowthToday.diedFromAge);
        AddDiedString(ref additionalBaseText, "diedFromWars", populationGrowthToday.diedFromWars);
        AddDiedString(ref additionalBaseText, "diedFromMurders", populationGrowthToday.diedFromMuders);
        AddDiedString(ref additionalBaseText, "diedFromEpidemies", populationGrowthToday.diedFromEpidemies);
        AddDiedString(ref additionalBaseText, "diedFromSuicide", populationGrowthToday.diedFromSuicide);

        if (Reich.instance.epidemies.Count > 0)
        {
            additionalBaseText.text += "\n" + Localization.GetString("newDay_additional_population_epidemies") + ": ";
            foreach (Epidemy epidemy in Reich.instance.epidemies)
            {
                additionalBaseText.text += epidemy.selfName + "; ";
            }
        }
    }

    private void AddDiedString(ref TextMeshProUGUI result, string key, int number)
    {
        if (number > 0)
        {
            result.text += "\n" + Localization.GetString("newDay_additional_population_" + key) + ": " + number.ToString();
        }
    }

    public void CloseAdditionalInfo()
    {
        LevelAudio.instance.Play(SoundType.ClickDecline);
        additionalInfoMenu.SetActive(false);
    }
}