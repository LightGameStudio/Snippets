using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;


public class Diary : AutoCleanupSingleton<Diary>
{
    public bool canLogActions = false;
    private List<ActionDiaryRecording> recordings = new List<ActionDiaryRecording>();
    private List<LogDiaryRecording> logs = new List<LogDiaryRecording>();


    public void LogNewAction(string description, AState state)
    {
        if (canLogActions == false)
        {
            return;
        }

        recordings.Add(new ActionDiaryRecording(description, Date.Instance.Day, state));
    }

    public void Log(string description)
    {
        logs.Add(new LogDiaryRecording(description, Date.Instance.Day));
    }


    public string Get(DiaryMode renderMode) //Neutral is Global
    {
        string result = "";
        int lastRecordedDay = 0;


        if (renderMode == DiaryMode.Global)
        {
            foreach (ActionDiaryRecording recording in recordings)
            {
                GetRecording(recording, lastRecordedDay, ref result);
            }
        }


        else if (renderMode == DiaryMode.Local)
        {
            foreach (ActionDiaryRecording recording in recordings)
            {
                if (recording.state == Reich.instance || recording.state == NeutralTerritory.instance)
                {
                    GetRecording(recording, lastRecordedDay, ref result);
                }
            }
        }


        else if (renderMode == DiaryMode.Log)
        {
            foreach (LogDiaryRecording recording in logs)
            {
                if (recording.day > lastRecordedDay)
                {
                    result += Localization.GetString("date_day") + " " + recording.day.ToString() + ":\n";
                    lastRecordedDay = recording.day;
                }
                result += recording.time + ": ";
                result += recording.log;
                result += "\n";
            }
        }
        return result;
    }

    private void GetRecording(ActionDiaryRecording recording, int lastRecordedDay, ref string result)
    {
        if (recording.day > lastRecordedDay)
        {
            result += Localization.GetString("date_day") + " " + recording.day.ToString() + ":\n";
            lastRecordedDay = recording.day;
        }

        if (recording.state != NeutralTerritory.instance)
        {
            result += recording.state.GetSelfName() + ": ";
        }
        result += recording.log;
        result += "\n";
    }
}

public abstract class DiaryRecording
{
    public int day;
    public string log;

}


public class ActionDiaryRecording : DiaryRecording
{
    public AState state;

    public ActionDiaryRecording(string log, int day, AState state)
    {
        this.state = state;
        this.log = log;
        this.day = day;
    }
}

public class LogDiaryRecording : DiaryRecording
{
    public string time;

    public LogDiaryRecording(string log, int day)
    {
        this.log = log;
        this.day = day;
        time = System.DateTime.Now.ToLongTimeString();
    }
}