﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text.RegularExpressions;
using System;

public class Localization : MonoBehaviour
{
    public static bool localizationFileIsRead = false;

    private static Dictionary<string, string> localizedText = new Dictionary<string, string>();
    private static Dictionary<string, string> localizedArticles = new Dictionary<string, string>();

    private static string missingString = "Translation error";

    private static TextAsset currentLocalizationText;
    private static bool initializedLanguage = false;
    private static bool initializedArticles = false;
    private static bool clearedArticles = false;

    public static Action LanguageChangedAction;


    public void SetLanguage(int value)
    {
        //PlayerPrefs.SetInt(Settings.locationKey, (int)value);
        Settings.CurrentLanguage = (SystemLanguage)value;

        StartCoroutine(ReadAllStrings());

        RelocalizeTextMeshes();
    }

    private void Awake()
    {
        if (initializedLanguage == false)
        {
            if (PlayerPrefs.HasKey(Settings.locationKey) == true)
            {
                SetLanguage(PlayerPrefs.GetInt(Settings.locationKey, (int)Application.systemLanguage));
            }
            else
            {
                SetDefaultLanguage();
                StartCoroutine(ReadAllStrings());
            }
            initializedLanguage = true;
        }
        DontDestroyOnLoad(transform.root.gameObject);

    }

    public bool CheckArticlesInitialized()
    {
        if (clearedArticles)
        {
            return true;
        }
        if (localizedArticles.Count == 0)
        {
            return false;
        }
        return true;
    }

    private IEnumerator CheckArticle(float time)
    {
        yield return new WaitForSeconds(time);
        if (CheckArticlesInitialized() == false)
        {
            ReadArticles();
        }
    }

    private IEnumerator CheckLocalization(float time)
    {
        yield return new WaitForSeconds(time);
        if (localizedText.Count == 0)
        {
            StartCoroutine(ReadAllStrings());
        }
    }


    public static void SetDefaultLanguage()
    {
        if (Application.systemLanguage == SystemLanguage.Russian ||
        Application.systemLanguage == SystemLanguage.Ukrainian ||
        Application.systemLanguage == SystemLanguage.Belarusian)
        {
            Settings.CurrentLanguage = SystemLanguage.Russian;
        }
        else
        {
            Settings.CurrentLanguage = SystemLanguage.English;
        }
    }

    private static void RelocalizeTextMeshes()
    {
        LanguageChangedAction?.Invoke();
    }

    private IEnumerator ReadAllStrings()
    {
        localizedText.Clear();
        ReadStringsFromFile("ui", localizedText);
        yield return new WaitForSecondsRealtime(0.03f);
        ReadStringsFromFile("main", localizedText);
        //yield return new WaitForSecondsRealtime(1.8f);
        StartCoroutine(CheckLocalization(3f));
    }

    public void ReadArticles()
    {
        // Debug.Log("ReadArticles");
        localizedArticles.Clear();
        ReadStringsFromFile("articles", localizedArticles);
        StartCoroutine(CheckArticle(7f));
        FindObjectOfType<NewsGeneartor>().ReadArticlesFromFile();
    }

    private void ReadStringsFromFile(string prefix, Dictionary<string, string> dictionary)
    {
        // Debug.Log("ReadLanguageStringsFromFile " + prefix);
        currentLocalizationText = (TextAsset)Resources.Load("Localization/" + prefix + "_" + Settings.CurrentLanguage.ToString(), typeof(TextAsset));

        if (currentLocalizationText != null)
        {
            string[] lines = currentLocalizationText.text.Split(new string[] { "\r\n", "\n\r", "\n" }, System.StringSplitOptions.RemoveEmptyEntries);

            for (int i = 0; i < lines.Length; i++)
            {
                string[] pairs = lines[i].Split(new char[] { '\t', '=' }, 2);
                if (pairs.Length == 2)
                {
                    dictionary.Add(pairs[0].Trim(), pairs[1].Trim());

                }
            }
            // Debug.Log("Finished reading " + prefix + " Count is " + dictionary.Count);
            localizationFileIsRead = true;
        }
        else
        {
            Debug.LogErrorFormat("Locale " + prefix + " not found!");
        }
    }

    public static string GetUIString(string key)
    {
        return GetString("ui_" + key);
    }

    public static string GetString(string key)
    {
        if (localizationFileIsRead == false)
        {
            Debug.LogErrorFormat("LanguageController hasn't been initialized while reading " + key);
            return missingString;
        }

        string result = missingString;
        if (localizedText.ContainsKey(key))
        {
            result = localizedText[key].Replace(@"\n", "" + '\n');
        }
        else
        {
            Debug.LogErrorFormat("File doesn't contains key " + key);
        }
        return result;
    }

    public static string GetArticle(string key)
    {
        string result = missingString + " " + key;
        if (localizedArticles.ContainsKey(key))
        {
            result = localizedArticles[key].Replace(@"\n", "" + '\n');
        }
        else
        {
            Debug.LogErrorFormat("File doesn't contains key " + key);
        }
        return result;
    }

    public static void ClearArticles()
    {
        clearedArticles = true;
        localizedArticles.Clear();
    }
}