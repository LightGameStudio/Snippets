﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

[RequireComponent(typeof(TextMeshProUGUI))]
public class LocalizeTextMesh : MonoBehaviour
{
    [SerializeField] private string key;
    private void Start()
    {
        Localize(0.1f);
    }

    private void OnEnable()
    {
        Localization.LanguageChangedAction += Relocalize;
    }
    private void OnDisable()
    {
        Localization.LanguageChangedAction -= Relocalize;
    }

    private IEnumerator LocalizeCoroutine(float deltaTime)
    {
        yield return new WaitForSecondsRealtime(deltaTime);
        Localize(deltaTime);
    }

    private void Localize(float deltaTime)
    {
        if (Localization.localizationFileIsRead == true)
        {
            GetComponent<TextMeshProUGUI>().text = Localization.GetString(key);
        }
        else
        {
            Debug.Log("Localized String is still not available: " + key);
            GetComponent<TextMeshProUGUI>().text = "Translating...";
            StartCoroutine(LocalizeCoroutine(deltaTime * 1.4f));
        }
    }

    public void Relocalize()
    {
        Localize(0.001f);
    }
}
