﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum DayTime { Day, Night };

public class CircularMovement : MonoBehaviour
{
    [SerializeField] private Sprite Sun;
    [SerializeField] private Sprite Moon;

    [SerializeField] private Transform center;

    private float angularSpeed;
    private float radius;
    private float angle = 0f;
    private Vector2 position;
    private SpriteRenderer spriteRenderer;
    private DayTime dayState = DayTime.Day;
    private const float timeToSwitch = 2f;
    private const float lowPointAngle = 3.1f;
    private const float floatCircleY = 0.72f;

    private void Start()
    {
        angularSpeed = Weather.changingSkyColorSpeed * 3.02385f;

        spriteRenderer = GetComponent<SpriteRenderer>();
        radius = Vector2.Distance(transform.position, center.transform.position);

    }

    private void Update()
    {
        if (Weather.doShowEffects)
        {
            SetCircularPosition();
        }
    }

    private void SetCircularPosition()
    {
        position.x = center.position.x - Mathf.Cos(angle) * radius;
        position.y = center.position.y + Mathf.Sin(angle) * radius * floatCircleY;

        transform.position = position;
        angle += angularSpeed * Time.deltaTime;

        if (angle >= lowPointAngle)
        {
            HideSun();
        }
    }

    private void HideSun()
    {
        Weather.doShowEffects = false;
        spriteRenderer.sprite = null;

        StartCoroutine(RetartCycle());
    }
    
    private IEnumerator RetartCycle()
    {
        yield return new WaitForSeconds(timeToSwitch);

        angle = 0f;
        SetCircularPosition();

        if (dayState == DayTime.Day)
        {
            dayState = DayTime.Night;
            spriteRenderer.sprite = Moon;
        }
        else if (dayState == DayTime.Night)
        {
            dayState = DayTime.Day;
            spriteRenderer.sprite = Sun;
        }
        Weather.doShowEffects = true;
    }
}
